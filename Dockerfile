FROM tboquet/anacuda7hc5:latest

RUN apt-get update --fix-missing && apt-get install -y \
    libopenblas-dev libhdf5-dev graphviz graphviz-dev --no-install-recommends && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN pip install brewer2mpl celery sqlalchemy opencv-python flower && \
    conda install -y cython \
    nose numpy scipy six \
    pandas scikit-learn matplotlib \
    ipython jupyter h5py pyyaml pytables \
    tqdm \
    seaborn yt sympy patsy \
    statsmodels pymongo && \
    pip install ggplot pydot-ng

# Install Theano:
RUN pip install --upgrade --no-deps git+git://github.com/Theano/Theano.git

# Install Tensorflow:
RUN pip install tensorflow-gpu

# Install Keras: 
RUN cd /root && git clone https://github.com/fchollet/keras.git && cd keras && \
    python setup.py install

RUN mkdir -p -m 700 /root/.jupyter/ && \
    echo "c.NotebookApp.ip = '*'" >> /root/.jupyter/jupyter_notebook_config.py

EXPOSE 8888

RUN echo "[global]\ndevice=gpu\nfloatX=float32\n[nvcc]\nfastmath=True" > /root/.theanorc

WORKDIR "/root"

CMD ["/bin/bash"]



