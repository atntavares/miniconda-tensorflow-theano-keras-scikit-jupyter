# miniconda-tensorflow-theano-keras-scikit-jupyter

cuda 7.5 + cudnn5-devel + miniconda + tensorflow + theano + keras + scikit + jupyter, running nvidia-docker on local GPUs.

# Requirements

- [nvidia-docker](https://github.com/NVIDIA/nvidia-docker)
- one or more NVIDIA gpu(s) with cuda compute capabilities > 3.0
- CUDA driver installed on the Host OS

# Usage

If you want to use your GPU 0 and GPU 1 (as listed by nvidia-smi), be able to serve a jupyter notebook via the port 8888 and mount a volume where some notebooks are located use:

```
NV_GPU='0,1' nvidia-docker run -it -p 8888:8888 -v ~/notebooks:/notebooks atavares/miniconda-tensorflow-theano-keras-scikit-jupyter

```

